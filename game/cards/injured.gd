extends "card_base.gd"

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func validate_args():
	if self.arguments.size() == 1:
		var a = self.arguments[0]
		if a == self.game.get_node('areas/sixbay'):
			if a.activated:
				return true
	return false
	
func validate_argument(node):
	if node.is_in_group('areas'):
		return true
	return false
	
func _proc_effects():
	self.game.add_card_to_deck()
	self.game.destroy_card(self)
	
func _card_help():
	return 'Use on activated six bay to remove this card and get a new card put in your deck. Arguments %d / %d' %[self.arguments.size(), self.args_required_to_play]