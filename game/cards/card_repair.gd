extends "card_base.gd"

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func validate_args():
	if self.arguments.size() == 1:
		var n = self.arguments[0]
		if n.health < n.max_health:
			return true
	return false

func validate_argument(node):
	if node.is_in_group('areas'):
		return true
	return false
	
func _proc_effects():
	var n = self.arguments[0]
	n.set_health(n.health + 1)