extends "card_base.gd"

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func validate_args():
	if self.arguments.size() == 1:
		var a = self.arguments[0]
		# @Bug Shady shit which will break
		if a.is_in_group('cards') && a.display_name == 'Injured Crewmember':
			return true
	return false

func validate_argument(node):
	if node.is_in_group('cards'):
		return true
	return false
	
func _proc_effects():
	self.game.destroy_card(self.arguments[0])
	
func _card_help():
	return 'Use on an injured crewmember in your hand to heal them and remove the card from the game. Arguments %d / %d' %[self.arguments.size(), self.args_required_to_play]