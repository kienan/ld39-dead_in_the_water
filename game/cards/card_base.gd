extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export(bool) var confirm_state = 0
export(String) var display_name = ''
export(String) var help = ''
var debug = false
var previous_location
var playing = 0
var play_timer = 0.0
var play_done_callback
var game
var waiting_args = false
var arguments = []
export(int) var args_required_to_play = 0

var click_target = null
var click_results = []

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process_input(true)
	set_process(true)
	#set_fixed_process(true)
	if self.debug:
		get_node('debug').set_text('%s' % self)
		get_node('debug').show()

func _fixed_process(dt):
	if self.click_target == null:
		return
	var results = get_world_2d().get_direct_space_state().intersect_point(self.click_target)
	print(results)
	self.click_results = click_results
	self.click_target = null

func _process(dt):
	if self.waiting_args == 1 && self.args_required_to_play <= 0:
		self.end_card_play(true)
	if self.playing:
		self.play_timer -= dt
	if self.play_timer <= 0 && self.playing:
		self.end_card_play(true)

func validate_args():
	return false
	

func on_waiting_args():
	if self.args_required_to_play <= 0:
		return
	self.set_scale(Vector2(1, 1))
	self.set_global_pos(Vector2(self.get_viewport().get_rect().size.x-50, 65))
	self.set_help(true)

func set_help(enable):
	if enable:
		self.game.get_node('ui/help').set_text(self._card_help())
	else:
		self.game.get_node('ui/help').set_text('')
		
func _card_help():
	return "%d / %d targets required to play this card. Select then hit enter to confirm. Escape to cancel" % [self.arguments.size(), self.args_required_to_play]

func _input(event):
	if not self.is_visible():
		return
	if event.type == InputEvent.KEY && event.scancode == KEY_ESCAPE:
		if self.confirm_state == 1:
			self.end_confirm_state(false)
		if self.waiting_args:
			self.end_card_play(false)
	if event.type == InputEvent.KEY && (event.scancode == KEY_RETURN || event.scancode == KEY_ENTER):
		if self.confirm_state == 1:
			self.end_confirm_state(true)
		if self.waiting_args:
			if self.validate_args():
				self.end_card_play(true)

func _on_game_area_clicked(area):
	if not self.waiting_args:
		return
	self.add_card_arg(area)

func add_card_arg(node):
	if not self.validate_argument(node):
		return
	var i = self.arguments.find(node)
	if i != -1:
		# Already in arguments. De-select instead
		self.arguments[i].set_hilight(false)
		self.arguments.remove(i)
		self.set_help(true)
		return
	if self.arguments.size() >= self.args_required_to_play:
		var a = self.arguments[0]
		a.set_hilight(false)
		self.arguments.pop_front()
	self.arguments.append(node)
	node.set_hilight(true)
	self.set_help(true)

func validate_argument(node):
	pass

func enter_confirm_state():
	if confirm_state == 1:
		return
	if self.game:
		var hand = self.game.get('hand')
		self.game.get_node('ui/help').set_text('Press "Enter" to play the card; Press "Escape" to cancel')
		for i in hand:
			if i.confirm_state == 1:
				i.end_confirm_state(false);
	confirm_state = 1
	self.previous_location = self.get_global_pos()
	var r = get_viewport().get_visible_rect()
	var c = Vector2(r.pos.x + (r.size.x / 2), r.pos.y + (r.size.y / 2))
	self.set_global_pos(c)
	self.set_scale(Vector2(3, 3))
	self.set_z(self.get_z()+1)
	
	#get_node('Controls').show()
	
func end_confirm_state(play_card):
	if confirm_state == 0:
		return
	confirm_state = 0
	self.set_scale(Vector2(1, 1))
	self.set_global_pos(self.previous_location)
	self.set_z(self.get_z()-1)
	get_node('Controls').hide()
	if self.game:
		self.game.get_node('ui/help').set_text('')
		if play_card:
			self.game.play_card(self)

func _on_click_base_input_event( viewport, event, shape_idx ):
	if not self.is_visible():
		return
	if event.type == InputEvent.MOUSE_BUTTON && event.button_index == BUTTON_LEFT && event.pressed:
		pass
	else:
		return
	# Possibly being chosen by another card
	if self.game.game_state == 1:
		var c
		var card
		var cards = self.game.get_tree().get_nodes_in_group('cards')
		for c in cards:
			if c == self:
				continue
			if ! c.is_visible():
				continue
			if c.waiting_args == 1:
				card = c
		if card:
			card.add_card_arg(self)
	if self.game.game_state != 0:
		return
	if confirm_state != 1:
		self.enter_confirm_state()
		return self
			
func start_card_play(callback = null):
	print("Starting play of %s" % self.display_name)
	if callback:
		self.play_done_callback = callback
	self.playing = 0
	self.waiting_args = 1
	self.on_waiting_args()
	
func end_card_play(play = false):
	print("Ending play of %s (%s)" % [ self.display_name, play])
	if play:
		self._proc_effects()
	self.set_global_pos(self.previous_location)
	self.playing = 0
	self.play_timer = 0.0
	self.waiting_args = 0
	for a in arguments:
		a.set_hilight(false)
	self.arguments = []
	if self.game && self.play_done_callback:
		self.game.call(self.play_done_callback, self, play)
	if self.game:
		self.game.get_node('ui/help').set_text('')

func set_game(game):
	self.game = game
	# @TODO maybe a signal from the game when areas are added/removed
	var areas = self.game.get_tree().get_nodes_in_group('areas')
	for area in areas:
		area.connect('game_area_clicked', self, '_on_game_area_clicked')

func _proc_effects():
	pass

func set_hilight(enable):
	if enable:
		get_node('Sprite').set_modulate(Color(0, 1, 0, 1))
	else:
		get_node('Sprite').set_modulate(Color(1, 1, 1, 1))