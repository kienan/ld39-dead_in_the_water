extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass


func _on_game_game_deck_changed(count):
	get_node('deckCount').set_text("Cards in deck: %d" % count)



func _on_game_game_discard_changed(count):
	get_node('discardCount').set_text("Cards in discard: %d" % count)


func _on_game_game_hand_changed(count):
	get_node('handCount').set_text("Cardss in hand: %d" % count)
