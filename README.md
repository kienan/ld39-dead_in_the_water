# Dead in the Water - LD39 Game

A game produced for [Ludum Dare 39](http://ldjam.com/events/ludum-dare/39) with the theme "Running out of Power"

Your ship has been badly damaged and the reactor is broken. You must repair your reactor before you run
out of energy. Failure to do so will leave you dead, drifting throughout space of aeons to come.

# LICENSE

Source assets (under the directory source\_assets/) are licensed under CC-BY-SA 4.0, see source\_assets/LICENSE.txt for the complete wording.

Code and project files are otherwised licensed under GPLv3, see LICENSE.txt for the complete text.

Copyright 2017 Kienan Stewart

